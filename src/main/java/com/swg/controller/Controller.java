/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swg.controller;

import com.swg.service.ServiceLayer;
import com.swg.view.View;

/**
 *
 * @author gabe
 */
public class Controller {

    private ServiceLayer serv;
    private View view;

    public Controller(ServiceLayer serv, View view) {
        this.serv = serv;
        this.view = view;

    }

    public void run() {

        view.printMenu();
    }
}
