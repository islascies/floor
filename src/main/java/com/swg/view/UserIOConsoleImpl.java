/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swg.view;

import java.math.BigDecimal;
import java.util.Scanner;

/**
 *
 * @author gabe
 */
public class UserIOConsoleImpl implements UserIO {

    private Scanner scan = new Scanner(System.in);

    @Override
    public void print(String msg) {
        System.out.println(msg);
    }

    public void printS(String msg) {
        System.out.print(msg);
    }

    @Override
    public BigDecimal readBigDecimal(String prompt) {
        System.out.println(prompt);
        BigDecimal returnValue = scan.nextBigDecimal();
        scan.nextLine();
        return returnValue;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        do {
            System.out.println(prompt);
            double returnValue = scan.nextDouble();
            scan.nextLine();
            if (returnValue >= min && returnValue <= max) {
                return returnValue;
            }
        } while (true);
    }

    @Override
    public float readFloat(String prompt) {
        System.out.println(prompt);
        float returnValue = scan.nextFloat();
        scan.nextLine();
        return returnValue;
    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        do {
            System.out.println(prompt);
            float returnValue = scan.nextFloat();
            scan.nextLine();
            if (returnValue >= min && returnValue <= max) {
                return returnValue;
            }
        } while (true);
    }

    @Override
    public int readInt(String prompt) {
        System.out.println(prompt);
        int returnValue = scan.nextInt();
        scan.nextLine();
        return returnValue;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int returnValue;
        do {

            do {
                System.out.print("Make a choice: ");
                while (!scan.hasNextInt()) {
                    String input = scan.next();
                    System.out.printf("\"%s\" is not a valid number.\n", input);
                    System.out.print("Make a choice: ");
                }

                returnValue = scan.nextInt();
                scan.nextLine();
                if (returnValue >= min && returnValue <= max) {
                    return returnValue;
                } else {
                    System.out.println("Please enter a number listed on the menu.");
                }
            } while (returnValue < 0);

        } while (true);

    }

    @Override
    public long readLong(String prompt) {
        System.out.println(prompt);
        long returnValue = scan.nextLong();
        scan.nextLine();
        return returnValue;
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        do {
            System.out.println(prompt);
            long returnValue = scan.nextLong();
            scan.nextLine();
            if (returnValue >= min && returnValue <= max) {
                return returnValue;
            }
        } while (true);
    }

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        return scan.nextLine();
    }

    @Override
    public double readDouble(String prompt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
