/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swg.view;

/**
 *
 * @author gabe
 */
public class View {

    private UserIO io;

    public View(UserIO io) {
        this.io = io;
    }

    public void printMenu() {
        io.print("MENU");
        io.print("1. Display Orders");
        io.print("2. Add an Order");
        io.print("3. Edit an Order");
        io.print("4. Remove an Order");
        io.print("5. Save Current Work");
        io.print(" 6. Quit");

    }

}
